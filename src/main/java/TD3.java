/**
 *
 * @param a
 * @param b
 * @return le max entre a et b
 */
int max2(int a, int b ) {

    if (a > b) {
        return a;
    } else {
        return b;
    }
}
/**
 *
 * @param a
 * @param b
 * @param c
 * @return le max entre a, b et c
 */
int max3(int a, int b , int c) {
    if(a>b && a>c){
        return a;
    }
    if(b>a && b>c){
        return b;
    }
    else{
        return c;
    }
}

/**
 *
 * @param n
 * @return le nombre de chiffres que comporte l'écriture, en base 10, de n
 */
int nbChiffres(int n ){
    int compteur = 1;
    while(n /10 != 0) {
        compteur++;
        n = n/10;
    }
    return compteur;
}

/**
 *
 * @param n
 * @return le nombre de chiffres que comporte l'écriture du nombre n^2
 */

int nbChiffresDuCarre(int n){
    return (nbChiffres(n*n));
}

/**
 * afficher nb caractères car (à partir de la position courante du curseur sur le terminal).
 * @param nb
 * @param car
 */
void  repetCarac(int nb, char car){
    for(int i = 0 ; i < nb ; i++){
        Ut.afficher(car);
    }
}

/**
 * affiche à l'écran une pyramide de hauteur h constituée de lignes répétant le caractère c.
 * @param h
 * @param c
 */
void pyramideSimple (int h  , char  c ){
    int nbEtoile=1;
    for(int i = 1; i<=h ; i ++){
        repetCarac(h-1, ' ');
        repetCarac(nbEtoile, c);
        Ut.sautLigne();
        nbEtoile += 2;
    }
}

/**
 * affiche à l'écran sur une même ligne les chiffres représentant les unités des nombres allant de nb1 à nb2 en ordre croissant
 * si nb1 < nb2, et ne fait rien sinon.
 * @param nb1
 * @param nb2
 */
void afficheNombresCroissants (int nb1,int nb2 ){
    while(nb1<=nb2){
        Ut.afficher(nb1 % 10);
        nb1++;
    }
}

/**
 * affiche à l'écran sur une même ligne les chiffres représentant les unités des nombres allant de nb1 à nb2 en ordre décroissant
 * si nb1 < nb2, et ne fait rien sinon.
 * @param nb1
 * @param nb2
 */
void afficheNombresDecroissants (int nb1,int nb2 ){
    while(nb1 >= nb2){
        Ut.afficher(nb2 % 10);
        nb2--;
    }
}

/**
 * permet de représenter à l'écran la pyramide
 * @param h
 */

void pyramide(int h ) {
    int espace = h-1;
    for(int i = 1; i<=h; i++){
        repetCarac(espace, ' ');
        afficheNombresCroissants(i, 2*i-1);
        afficheNombresDecroissants(i, 2*i-2);
        Ut.sautLigne();
        espace--;
    }

}

/**
 * @param c
 * @return la racine carrée entière n d'un nombre entier c donné,
 * si c est un carré parfait, c'est-à-dire si c = n * n.
 * Sinon la fonction retourne -1.
 */
int racineParfaite(int c ){
    int sqrt = (int) Math.sqrt(c);
    return (sqrt * sqrt == c) ? sqrt : -1;
}

/**
 *
 * @param nb
 * @return vrai si un entier donné est un carré parfait, faux sinon.
 */
boolean estCarreParfait(int nb){
    return racineParfaite(nb != -1);
}

/**
 *
 * @param p
 * @param q
 * @return vrai  si deux nombres entiers `p`et `q` sont amis, faux sinon.
 */
boolean nombresAmicaux(int p , int q){
    int diviseursP = 0;
    int diviseursQ = 0;
    boolean p = Ut.saisirBooleen();
    boolean q = Ut.saisirBooleen();

    for (int i = 1; i < p; i++) {
        if (p % i == 0) {
            diviseursP += i;
        }
    }
    for (int i = 1; i < q; i++) {
        if (q % i == 0) {
            diviseursQ += i;
        }
    }
    return diviseursP =p && diviseursQ = q;

    //V2
    int sommep = 0;
    for(int i = 1 ; i<=p ; i ++){
        if(p%=0 && i!=q){
            sommep +=i;
        }
    }
    int sommeq = 0;
    for(int i = 1; i <=q ; i++){
        if(q%i==0 && i!= p){
            sommeq+=i;
        }
    }
}

// penser à faire une sous fonction utiliser dans le programme
// comme dans nombres amis, ne pas utiliser deux fois la même fonctions mais en creer une avant et la reutiliser dqns
//la fonction de départ

/**
 * affiche à l'écran les couples de
 *nombres amis parmi les nombres inférieurs ou égaux à max.
 * @param max
 */

void afficheAmicaux(int max){
    for (int i = 1; i <= max; i++) {
        for (int j = i + 1; j <= max; j++) {
            if (nombresAmicaux(i, j)) {
                Ut.afficherSL(i + " et " + j + " sont amis.");
            }
        }
    }
}

/**
 *
 * @param c1
 * @param c2
 * @return vrai si deux entiers donnés peuvent  être les côtés de l'angle droit d'un triangle rectangle dont les 3
 *        côtés sont des nombres entiers, faux sinon
 */
boolean  estTriangleRectangle (int c1, int c2){
    int hypotenuseSquared = c1 * c1 + c2 * c2;
    int hypotenuse = (int) Math.sqrt(hypotenuseSquared);
    return (hypotenuse * hypotenuse == hypotenuseSquared);
}

void main (){
   estTriangleRectangle(5,6);
}